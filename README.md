## SHINE: Solver for Hydromagnetic INviscid modes in Ellipsoids##


SHINE is a numerical code written by Dr Jérémie Vidal (https://sites.google.com/view/jvidalhome), under the [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible with GNU GPL). 

FEATURES:
---------

The code computes the **global** diffusionless hydromagnetic eigenmodes of **incompressible** or **compressible** fluids in co-rotating **triaxial ellipsoids**. SHINE handles **polynomial** and **ellipsoidal** perturbations of **unprecedented** spatial complexity. 

**Two independent versions** of SHINE are provided for reasearch purposes. 

* **v1** extends the pioneering algorithm of [Vantieghem (2014)](https://royalsocietypublishing.org/doi/full/10.1098/rspa.2014.0093), in seeking **incompressible** **hydromagnetic** modes that admit **exact polynomial** structures. 
Only well-chosen background magnetic fields are allowed, see in [Vidal (PhD thesis, 2018)](https://drive.google.com/open?id=1rUXyjQWz9sc9A5kFd0vYf0pBqOEbjR38) for further details.

* **v2** includes a novel **Galerkin** formulation to unlock the previous limitations, to consider either an arbitrary **magnetic** and **stratified** background state (under the **Boussinesq** approximation), or a **fully compressible** formulation of the modal problem.

To use SHINE for research purposes, please **always cite** [Vidal et al. (JFM, 2020)](https://doi.org/10.1017/jfm.2019.1004), and also 

- [Vidal et. al (A&A, 2019)](https://doi.org/10.1051/0004-6361/201935658) for **Boussinesq** fluids.
- [Vidal & Cébron (PRSA, 2020)](https://doi.org/10.1098/rspa.2020.0131) for **compressible** planets.


USING SHINE:
-----------

- Download the appropriate .zip version in the `Downloads` tab.
- Python 3 with [Numpy](http://www.numpy.org/), [Scipy](https://www.scipy.org/) and [Sympy](http://www.sympy.org/fr/).
- [mpi4py](https://pypi.python.org/pypi/mpi4py) to build and solve the large eigenvalue problems (embarassingly parallel).

All the required Python libraries are included within [Anaconda](https://www.anaconda.com/download/#linux), a user-freiendly python distribution which does not require root access to be installed.

For help, please send me a mail.
